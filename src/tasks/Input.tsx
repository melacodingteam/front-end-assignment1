import React, { useState } from 'react';

import { View } from '../ui';

import useStyles from './Input.styles';

const InputPopup = ({
  error = undefined,
  label = '',
  LeftIcon = undefined,
  RightIcon = undefined,
  placeholder = '',
  type = '',
  value = '',
  onChange = undefined,
  func = undefined,
  passwordShown = false,
}) => {
  const styles = useStyles();

  const [focused, setFocused] = useState(false);

  const labelStyle = !!(focused || value);

  return (
    <View className={type === 'password' || passwordShown ? styles.inputGroupPsw : styles.inputGroup}>
      <label
        className={`${labelStyle ? styles.labelFocused : styles.label} ${styles.genericLabel}`}
        htmlFor="input"
      >
        {label}
      </label>

      <View className={`${styles.inputContainer} ${error ? styles.orangeBorder : null}`}>
        <View className={styles.svgIcon}>
          {LeftIcon && <LeftIcon focused={focused} filled={value} />}
        </View>

        <input
          className={styles.input}
          placeholder={placeholder}
          type={type}
          onChange={({ target: { value: text } }) => onChange && onChange(text)}
          value={value}
          onFocus={() => setFocused(true)}
          onBlur={() => setFocused(false)}
          autoComplete="new-password"
        />

        <View className={styles.svgIcon}>
          {RightIcon && <RightIcon func={func} passwordShown={passwordShown} />}
        </View>
      </View>
    </View>
  );
};

export default InputPopup;
