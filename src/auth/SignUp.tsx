import React, { useState, useEffect, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import LoadingOverlay from 'react-loading-overlay';
import { View } from '../ui';

import Input from './Input';

import Footer from './Footer';

import BackArrow from '../svgs/BackArrow';

import CheckIcon from '../svgs/CheckIcon';

import CheckIconSmall from '../svgs/CheckIconSmall';

import ToggleShowPassword from '../svgs/ToggleShowPassword';

import WhiteInputSpace from '../svgs/WhiteInputSpace';

import useStyles from './SignUp.styles';


const SignUp = () => {
  const history = useHistory();
  const styles = useStyles();
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [email, setEmail] = useState();
  const [emailConfirmation, setEmailConfirmation] = useState();
  const [password, setPassword] = useState('');
  const [acceptAgreement, setAcceptAgreement] = useState(false);
  const [pswValidation, setPswValidation] = useState(false);
  const [formIsValid, setFormIsValid] = useState(false);
  const [emailIsConfirmed, setEmailIsConfirmed] = useState(false);
  const [passwordShown, setPasswordShown] = useState(false);
  const [isLoading/*, setIsLoading*/] = useState(false);

  const validatePsw = useCallback(() => {
    if (password.match(/^(?=.{8,})((?=.*\d)(?=.*[a-z])(?=.*[A-Z])|(?=.*\d)(?=.*[a-zA-Z])(?=.*[\W_])|(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_])).*/)) {
      setPswValidation(true);
      return true;
    }
    setPswValidation(false);
    return false;
  }, [password]);

  const isEmailConfirmed = useCallback(() => {
    if ((email === emailConfirmation || emailConfirmation === undefined)) {
      return true;
    }
    return false;
  }, [email, emailConfirmation]);

  const toggleAcceptAgreement = () => {
    setAcceptAgreement(!acceptAgreement);
  };

  const goBack = () => {
    history.push(`/`);
  };

  const togglePasswordVisiblity = () => {
    setPasswordShown(!passwordShown);
  };

  const isFormValid = useCallback(() => password
    && validatePsw()
    && firstName
    && lastName
    && email
    && /\S+@\S+\.\S+/.test(email)
    && emailConfirmation
    && /\S+@\S+\.\S+/.test(emailConfirmation)
    && isEmailConfirmed
    && acceptAgreement,
  [acceptAgreement,
    email,
    emailConfirmation,
    firstName,
    lastName,
    password,
    isEmailConfirmed,
    validatePsw]);

  const handleSubmit = (e) => {
    e.preventDefault();

    history.push(`/registrationCompleted?email=${email}`);
  };

  useEffect(() => {
    if (isFormValid() && !formIsValid) {
      setFormIsValid(true);
    } else if (!isFormValid() && formIsValid) {
      setFormIsValid(false);
    }
  }, [isFormValid, formIsValid]);

  useEffect(() => {
    if (isEmailConfirmed() && !emailIsConfirmed) {
      setEmailIsConfirmed(true);
    } else if (!isEmailConfirmed() && emailIsConfirmed) {
      setEmailIsConfirmed(false);
    }
  }, [isEmailConfirmed, emailIsConfirmed]);

  return (
    <LoadingOverlay
      active={isLoading}
      spinner
      text=""
      styles={
        {
          wrapper: {
            flex: 1,
            display: 'flex',
            flexDirection: 'column',
          },
        }
      }
    >
      <View className={styles.container}>
        <View className={styles.containerLeft}>
          <View className={styles.innerContainer}>
            <View className={styles.logo}></View>
            <View className={styles.backContainer} onClick={goBack}>
              <View>
                <BackArrow />
              </View>
              <View>
                <h3 className={styles.toUppercase}>Login</h3>
              </View>
            </View>
            <View>
              <h1>Insert your data</h1>
              {!emailIsConfirmed ? (
                <span className={styles.error}>
                  Email and email confirmation do not match
                </span>
              ) : null}
            </View>
          </View>
        </View>
        <View className={styles.containerRight}>
          <View className={styles.innerContainer}>
            <form onSubmit={handleSubmit}>
              <Input
                value={firstName}
                onChange={(text) => setFirstName(text)}
                label={'First Name'}
                placeholder={'Insert your first name'}
                type="text"
                LeftIcon={WhiteInputSpace}
              />
              <Input
                value={lastName}
                onChange={(text) => setLastName(text)}
                label={'Last Name'}
                placeholder={'Insert your last name'}
                type="text"
                LeftIcon={WhiteInputSpace}
              />
              <Input
                value={email}
                onChange={(text) => setEmail(text)}
                label={'Email'}
                placeholder={'Insert your email'}
                type="email"
                LeftIcon={WhiteInputSpace}
              />
              <Input
                value={emailConfirmation}
                onChange={(text) => setEmailConfirmation(text)}
                label={'Confirm Email'}
                placeholder={'Insert your email'}
                type="email"
                LeftIcon={WhiteInputSpace}
              />
              <View>
                <Input
                  passwordShown={passwordShown}
                  func={togglePasswordVisiblity}
                  value={password}
                  onChange={(text) => setPassword(text)}
                  label={'Password'}
                  placeholder={'Insert your password'}
                  type={passwordShown ? 'text' : 'password'}
                  RightIcon={ToggleShowPassword}
                  LeftIcon={WhiteInputSpace}
                />
                <View className={`${styles.flexRowFlexStartCenter} ${styles.marginLeft}`}>
                  <View>
                    <CheckIconSmall trigger={pswValidation} func={null} />
                  </View>
                  <View className={styles.validation}>
                    At least 8 characters and at least 3 of the following: upper case letters, lower case letters, numbers, special characters
                  </View>
                </View>
              </View>
              <View className={styles.flexRowFlexStartCenter}>
                <View>
                  <CheckIcon
                    trigger={acceptAgreement}
                    func={toggleAcceptAgreement}
                  />
                </View>
                <View className={styles.agreement}>
                  BY OPTING-IN, YOU ACKNOWLEDGE TERMS AND CONDITIONS, PRIVACY POLICY AND YOU AGREE TO THE USE OF PERSONAL DATA FOR THE PURPOSES DESCRIBED.
                </View>
              </View>
              <button
                type="submit"
                className={formIsValid ? styles.button1 : styles.button2}
              >
                Create your account
              </button>
            </form>
          </View>
        </View>
      </View>
      <Footer />
    </LoadingOverlay>
  );
};

export default SignUp;
