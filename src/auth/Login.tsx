import React, { useState } from 'react';

import { useHistory } from 'react-router-dom';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import LoadingOverlay from 'react-loading-overlay';

import Email from '../svgs/Email';
import Key from '../svgs/Key';

import { View } from '../ui';

import Input from './Input';
import Footer from './Footer';

import useStyles from './Login.styles';
import ToggleShowPassword from '../svgs/ToggleShowPassword';

const Login = () => {
  const history = useHistory();

  const styles = useStyles();

  const [isLoading/* , setIsLoading */] = useState(false);
  const [loadingText/* , setLoadingText */] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordShown, setPasswordShown] = useState(false);

  const onConfirmClick = (cb) => () => {
    if (cb) {
      cb();
    }
  };

  const onLogin = (e) => {
    e.preventDefault();
    confirmAlert({
      customUI: ({ onClose }) => (
        <div className={styles.dialogContainer}>
          <h1 className={styles.dialogTitle}>Info</h1>
          <p className={styles.dialogText}>
            You are logged in!
          </p>
          <button type="button" onClick={onConfirmClick(onClose)} className={styles.button1}>Ok</button>
        </div>
      ),
    });
  };

  const togglePasswordVisiblity = () => {
    setPasswordShown(!passwordShown);
  };

  const onRecover = () => {
    history.push('/recover');
  };

  const onSignUp = () => {
    history.push(`/signup`);
  };

  return (
    <>
      <LoadingOverlay
        active={isLoading}
        spinner
        text={loadingText}
        styles={
          {
            wrapper: {
              flex: 1,
              display: 'flex',
              flexDirection: 'column',
            },
          }
        }
      >
        <View className={styles.container}>
          <View className={styles.containerLeft}>
            <View className={styles.innerContainerLeft}>
              <View className={styles.logo}></View>
              <View>
                <h1>Sign In</h1>
                <form onSubmit={onLogin}>
                  <View>
                    <Input
                      value={email}
                      onChange={(text) => setEmail(text)}
                      label={'Email'}
                      placeholder={'Insert your email'}
                      type="email"
                      LeftIcon={Email}
                    />
                    <Input
                      passwordShown={passwordShown}
                      func={togglePasswordVisiblity}
                      value={password}
                      onChange={(text) => setPassword(text)}
                      label={'Password'}
                      placeholder={'Insert your password'}
                      type={passwordShown ? 'text' : 'password'}
                      LeftIcon={Key}
                      RightIcon={ToggleShowPassword}
                    />
                    <a className={styles.a} onClick={onRecover}>Forgot password?</a>
                  </View>
                  <button type="submit" className={styles.button1}>
                    Log In
                  </button>
                </form>
              </View>
            </View>
          </View>
          <View className={styles.containerRight}>
            <View className={`${styles.innerContainerRight} ${styles.contentCenter}`}>
              <h2 className={styles.titleRight}>Don't have an account yet?</h2>
              <h5 className={styles.description}>Register now and start collaborating with your team now!</h5>
              <a onClick={onSignUp} className={styles.button2}>
                Sign Up
              </a>
            </View>
          </View>
        </View>
        <Footer />
      </LoadingOverlay>
    </>
  );
};

export default Login;
