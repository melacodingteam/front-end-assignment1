import { createUseStyles } from 'react-jss';
import colors from '../constants/colors';

const styles = createUseStyles({
  logo: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: '92%',
  },
  container: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerContainer: {
    flex: 1,
    maxHeight: '716px',
    maxWidth: 428,
    alignItems: 'center',
    marginTop: '85px',
  },
  alignCenter: {
    textAlign: 'center',
    margin: 0,
    marginTop: '32px',
    marginBottom: '42px',
  },
  button1: {
    height: '3em',
    width: '204px',
    borderRadius: '25px',
    border: '1px solid #000000',
    backgroundColor: colors.button.background.secondary,
    color: colors.button.color.secondary,
    fontFamily: 'Poppins',
    fontSize: '14px',
    letterSpacing: '0.5px',
    lineHeight: '21px',
    textAlign: 'center',
    minWidth: '93px',
    outline: 'none',
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  button2: {
    height: '3em',
    width: '204px',
    borderRadius: '25px',
    backgroundColor: colors.button.background.primary,
    color: colors.button.color.primary,
    fontFamily: 'Poppins',
    fontSize: '14px',
    letterSpacing: '0.5px',
    lineHeight: '21px',
    textAlign: 'center',
    border: 'none',
    minWidth: '80px',
    maxWidth: '300px',
    outline: 'none',
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  flexRowSpaceCenter: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  paragraph: {
    fontSize: '16px',
    textAlign: 'center',
    margin: '1em',
  },
  email: {
    fontWeight: 'bold',
    textTransform: 'lowercase',
  },
  '@media (max-width: 768px)': {
    container: {
      height: '86.74vh',
    },
  },

});

export default styles;
