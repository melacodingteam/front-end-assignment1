import React from 'react';

import { useHistory } from 'react-router-dom';
import { View } from '../ui';

import Footer from './Footer';

import useStyles from './RegistrationCompleted.styles';

const RegistrationCompleted = () => {
  const email:String = new URLSearchParams(window.location.search).get('email');
  const history = useHistory();
  const styles = useStyles();

  const goToLogin = () => {
    history.push('/');
  };
  return (
    <>
      <View className={styles.container}>
        <View className={styles.innerContainer}>
          <View>
            <h1 className={styles.alignCenter}>
              Registration completed!
            </h1>
          </View>
          <View className={styles.paragraph}>
            We have sent the verification email to
            {' '}
            <span className={styles.email}>{email}</span>
          </View>
          <View className={styles.paragraph}>
            Check your mailbox and follow the instructions to activate your account
          </View>
          <View className={styles.flexRowSpaceCenter}>
            {/* <button
              type="submit"
              onClick={handleSubmit}
              className={styles.button1}
            >
              Resend Email
            </button> */}
            <button
              type="submit"
              onClick={goToLogin}
              className={styles.button2}
            >
              Enter
            </button>
          </View>
        </View>
      </View>
      <Footer />
    </>
  );
};

export default RegistrationCompleted;
