import { createUseStyles } from 'react-jss';

import colors from '../constants/colors';

const useStyles = createUseStyles({
  label: {
    color: '#3F3D56',
    fontFamily: 'Poppins',
    fontSize: '12px',
    letterSpacing: '0.43px',
    lineHeight: '18px',
    marginLeft: '0.9em',
  },
  labelFocused: {
    color: '#04a6f4',
    fontFamily: 'Poppins',
    fontSize: '12px',
    letterSpacing: '0.43px',
    lineHeight: '18px',
    marginLeft: '0.9em',
  },
  inputGroup: {
    marginBottom: 32,
  },
  inputGroupPsw: {
    marginBottom: '0',
  },
  inputContainer: {
    borderRadius: 25,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: colors.input.border,
    maxWidth: 315,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  inputContainerOnError: {
    borderRadius: 25,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#F47860',
    maxWidth: 300,
    flexDirection: 'row',
    backgroundColor: 'white',
  },
  input: {
    display: 'flex',
    flex: 1,
    backgroundColor: 'transparent',
    border: 0,
    outline: 0,
    color: '#3F3D56',
    fontFamily: 'Poppins',
    fontWeight: '200',
    fontSize: '12px',
    letterSpacing: '0.38px',
    lineHeight: '18px',
  },
  invalidCredentials: {
    margin: '0 0 2em 1em',
    fontSize: '12px',
    maxWidth: '17vw',
    color: '#F47860',
  },
  svgIcon: {
    flex: 0,
  },
  genericLabel: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  orangeBorder: {
    borderColor: '#F47860',
  },
});

export default useStyles;
