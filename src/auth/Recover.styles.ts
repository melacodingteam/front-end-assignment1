import { createUseStyles } from 'react-jss';
import colors from '../constants/colors';

const styles = createUseStyles({
  logo: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: '92%',
  },
  container: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerContainer: {
    flex: 1,
    maxHeight: '716px',
    maxWidth: 315,
    marginTop: '44px',
  },
  alignCenter: {
    textAlign: 'center',
    margin: 0,
    marginTop: '80px',
    marginBottom: '42px',
  },
  button1: {
    marginTop: '100px',
    alignSelf: 'center',
    minHeight: '3em',
    width: '315px',
    borderRadius: '25px',
    backgroundColor: colors.button.background.primary,
    color: colors.button.color.primary,
    fontFamily: 'Poppins',
    fontSize: '14px',
    letterSpacing: '0.5px',
    lineHeight: '21px',
    textAlign: 'center',
    border: 'none',
    minWidth: '80px',
    maxWidth: '300px',
    outline: 'none',
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  button2: {
    marginTop: '100px',
    alignSelf: 'center',
    minHeight: '3em',
    width: '315px',
    borderRadius: '25px',
    border: 'none',
    backgroundColor: colors.button.background.disabled,
    color: colors.button.color.secondary,
    fontFamily: 'Poppins',
    fontSize: '14px',
    letterSpacing: '0.5px',
    lineHeight: '21px',
    textAlign: 'center',
    minWidth: '80px',
    maxWidth: '300px',
    outline: 'none',
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  returnLabel: {
    margin: '14px',
    textAlign: 'center',
    color: '#3F3D56',
    fontSize: '12px',
    lineHeight: '18px',
    marginLeft: '0.9em',
    letterSpacing: '0.43px',
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  sentMsg: {
    textAlign: 'justify',
  },
  '@media (max-width: 768px)': {
    container: {
      height: '86.74vh',
    },
  },
});

export default styles;
