import React, { useState } from 'react';

import { View } from '../ui';

import Input from './Input';

import Footer from './Footer';

import Email from '../svgs/Email';

import useStyles from './Recover.styles';

const Recover = () => {
  const styles = useStyles();
  const [email, setEmail] = useState();
  const [sentEmail, setSentEmail] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    setSentEmail(email);
  };

  return (
    <>
      <View className={styles.container}>
        <View className={styles.innerContainer}>
          <View>
            <h1 className={styles.alignCenter}>
              Forgot password?
            </h1>
          </View>
          <form onSubmit={handleSubmit}>
            <Input
              value={email}
              onChange={(text) => setEmail(text)}
              label={'Email'}
              placeholder={'Insert your email'}
              type="email"
              LeftIcon={Email}
            />
            <button
              type="submit"
              className={email ? styles.button1 : styles.button2}
            >
              {'Recover Password'}
            </button>
          </form>
          {sentEmail ? (
            <h5 className={styles.sentMsg}>
              We sent an email to this address: 
              {' '}
              {sentEmail}
              {' '}
              If you can't find the email, please check your spam folder.
            </h5>
          ) : null}
          <a href="/" className={styles.returnLabel}>Back</a>
        </View>
      </View>
      <Footer />
    </>
  );
};

export default Recover;
