import { createUseStyles } from 'react-jss';

import colors from '../constants/colors';

const useStyles = createUseStyles({
  footer: {
    backgroundColor: colors.screen.footer,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: '13.26vh',
    color: colors.white,
    fontFamily: 'Poppins, sans-serif',
    fontWeight: 600,
    fontSize: 12,
    textTransform: 'uppercase',
  },
  footerContainer: {
    flexDirection: 'row',
    display: 'flex',
    flex: 1,
    justifyContent: 'space-around',
    maxWidth: '1140',
    alignItems: 'center',
  },
  footerLink: {
    color: '#fff',
    textDecoration: 'none',
    marginLeft: 8,
  },
  footerInnerContainer: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: '16',
    marginRight: '16',
  },
  footerBadge: {
    marginLeft: '16',
  },
  '@media (max-width: 768px)': {
    footerContainer: {
      flexDirection: 'column',
    },
  },
});

export default useStyles;
