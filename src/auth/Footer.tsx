import React from 'react';
import useStyles from './Footer.styles';

const Footer = () => {
  const styles = useStyles();

  return (
    <div className={styles.footer}>
      <div className={styles.footerContainer}>

        <a
          href="#foo"
          rel="noopener noreferrer"
          target="_blank"
          className={styles.footerLink}
        >
          
        </a>
        
        <div className={styles.footerInnerContainer}>
          <a
            href="#foo"
            rel="noopener noreferrer"
            target="_blank"
            className={styles.footerLink}
          >
            Terms of Service
          </a>
        </div>

      </div>
    </div>
  );
};

export default Footer;
