import { createUseStyles } from 'react-jss';
import colors from '../constants/colors';

const styles = createUseStyles({
  container: {
    flex: 1,
    flexDirection: 'row',
    overflowY: 'scroll',
  },
  containerLeft: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  innerContainer: {
    flex: 1,
    maxWidth: 315,
  },
  containerRight: {
    alignItems: 'flex-start',
    flex: 1,
    justifyContent: 'center',
    marginTop: 32,
  },
  backContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 64,
  },
  flexRowFlexStartCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16,
  },
  validation: {
    fontSize: '12px',
    fontWeight: '200',
    textAlign: 'justify',
    margin: '1em',
  },
  agreement: {
    fontSize: '12px',
    fontWeight: 'bold',
    textAlign: 'justify',
    margin: '1em',
  },
  button1: {
    alignSelf: 'center',
    height: '3em',
    width: '16em',
    borderRadius: '25px',
    backgroundColor: colors.button.background.primary,
    color: colors.button.color.primary,
    fontFamily: 'Poppins',
    fontSize: '14px',
    letterSpacing: '0.5px',
    lineHeight: '21px',
    textAlign: 'center',
    border: 'none',
    minWidth: '80px',
    maxWidth: '300px',
    outline: 'none',
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  button2: {
    alignSelf: 'center',
    height: '3em',
    width: '16em',
    borderRadius: '25px',
    border: 'none',
    backgroundColor: colors.button.background.disabled,
    color: colors.button.color.secondary,
    fontFamily: 'Poppins',
    fontSize: '14px',
    letterSpacing: '0.5px',
    lineHeight: '21px',
    textAlign: 'center',
    minWidth: '80px',
    maxWidth: '300px',
    outline: 'none',
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  toUppercase: {
    textTransform: 'uppercase',
  },
  logo: {
    marginTop: 32,
  },
  marginLeft: {
    marginLeft: '20px',
  },
  spacing: {
    height: '100px',
  },
  error: {
    color: '#F47860',
  },
  '@media (max-width: 768px)': {
    container: {
      flexDirection: 'column',
    },
    containerRight: {
      alignItems: 'center',
      marginBottom: 32,
    },
  },
});

export default styles;
