import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Login from './auth/Login';
import RegistrationCompleted from './auth/RegistrationCompleted';
import SignUp from './auth/SignUp';
import Recover from './auth/Recover';

// const getBasename = (path) => path.substr(0, path.lastIndexOf('/'));
const App = () => (
  <Router basename="login">
    <Switch>
      <Route exact path="/">
        <Login />
      </Route>
      <Route path="/registrationCompleted">
        <RegistrationCompleted />
      </Route>
      <Route path="/signup">
        <SignUp />
      </Route>
      <Route path="/recover">
        <Recover />
      </Route>
    </Switch>
  </Router>
);

export default App;
