const colors = {
  screen: {
    primary: '#fff',
    secondary: '#f9f9fb',
    footer: '#3f3d56',
  },
  white: '#fff',
  input: {
    border: '#E5E5EA',
  },
  menu: {
    background: '#fff',
    border: '#E5E5EA',
    name: 'rgba(1, 12, 58, .5)',
    role: '#3f3d56',
  },
  tasks: {
    right: '#F9F9FB',
  },
  button: {
    background: {
      primary: '#04A6F4',
      secondary: '#fff',
      disabled: '#E5E5EA',
    },
    color: {
      primary: '#fff',
      secondary: '#3F3D56',
    },
  },
};

export default colors;
