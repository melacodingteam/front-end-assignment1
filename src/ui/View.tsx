import React from 'react';
import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles({
  view: ({ style }) => ({
    display: 'flex',
    flexDirection: 'column',
    outline: 'none',
    ...style,
  }),
  pointer: {
    cursor: 'pointer',
    border: 0,
  },
});

const View = ({
  children = undefined,
  className = '',
  style = {},
  onClick = undefined,
}) => {
  const styles = useStyles(style);

  return onClick ? (
    <div
      className={`${styles.view} ${styles.pointer} ${className}`}
      onClick={onClick}
      onKeyDown={() => {}}
      role="button"
      tabIndex={0}
    >
      {children}
    </div>
  ) : (
    <div className={`${styles.view} ${className}`}>{children}</div>
  );
};

export default View;
