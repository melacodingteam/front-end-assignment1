import * as React from 'react';

const WhiteInputSpace = () => (
  <svg width={15} height={34} viewBox="0 0 15 34">
    <g fill="none" fillRule="evenodd">
      <path d="M10 6h24v24H10z" />
      <path
        d="M29.452 22.826L24.625 18l4.827-4.826v9.652zm-14.46.712l4.852-4.852 1.7 1.686a.503.503 0 00.711-.002l1.658-1.658 4.827 4.826H14.992zm-.712-.712V13.17l4.85 4.808-4.85 4.85zm14.46-10.364l-6.842 6.841-6.901-6.841H28.74zm.208-1.007H14.783c-.832 0-1.51.677-1.51 1.51v10.07c0 .83.675 1.51 1.51 1.51h14.165c.83 0 1.51-.675 1.51-1.51v-10.07c0-.83-.675-1.51-1.51-1.51z"
        fill="#FFFFFF"
      />
    </g>
  </svg>
);

export default WhiteInputSpace;
