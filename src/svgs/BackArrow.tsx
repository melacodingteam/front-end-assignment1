import * as React from 'react';

const BackArrow = ({ size = 1, fill = '#3F3D56' }) => (
  <svg width={34 * size} height={12 * size} viewBox="0 0 34 12">
    <g fill="none" fillRule="evenodd">
      <path d="M0-12h36v36H0z" />
      <path
        d="M14.789 10.987l-4.454-4.46a.745.745 0 010-1.057l4.454-4.457a.745.745 0 111.054 1.056l-3.17 3.175h13.052c.418 0 .754.337.754.756a.754.754 0 01-.754.756H12.673l3.17 3.175a.745.745 0 11-1.054 1.056z"
        fill={fill}
      />
    </g>
  </svg>
);

export default BackArrow;
