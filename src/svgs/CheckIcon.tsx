import * as React from 'react';

const CheckIcon = ({ size = 1, trigger, func }) => (
  <svg width={18 * size} height={18 * size} viewBox="0 0 18 18" onClick={func}>
    <g fillRule="nonzero" fill="none">
      <path d="M-3-3h24v24H-3z" />
      <path
        d="M7.515 12.852L4.091 9.428l.96-.96 2.467 2.466 5.207-5.207.96.96-6.17 6.165zM9 .272a8.727 8.727 0 100 17.455A8.727 8.727 0 0017.727 9 8.753 8.753 0 009 .273z"
        fill={trigger ? '#04A6F4' : '#E5E5EA'}
      />
    </g>
  </svg>
);

export default CheckIcon;
