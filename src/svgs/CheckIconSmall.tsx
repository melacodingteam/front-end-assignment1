import * as React from 'react';

const CheckIconSmall = ({ size = 1, trigger, func }) => (
  <svg width={13 * size} height={12 * size} viewBox="0 0 13 12" onClick={func}>
    <g fill="none" fillRule="evenodd">
      <path d="M-1.5-2h16v16h-16z" />
      <path
        d="M5.51 8.568L3.227 6.285l.64-.64 1.645 1.644 3.471-3.47.64.64L5.51 8.567zM6.5.182A5.818 5.818 0 1012.318 6 5.835 5.835 0 006.5.182z"
        fill={trigger ? '#04A6F4' : '#E5E5EA'}
      />
    </g>
  </svg>
);

export default CheckIconSmall;
