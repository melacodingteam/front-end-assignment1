# Frontend engineer
This assignment is meant to evaluate the React proficiency of full-time engineers. Your code structure should follow design best practices and our evaluation will focus primarily on your ability to follow good design principles and less on correctness and completeness of algorithms.

#### ⚠️ Do not mention Mela anywhere on the code or repository name.
#### ⚠️ Do not copy any files from this repository

## Time limits
The evaluation result of the test is not linked to how much time you spend on it. Please DO NOT spend more than 3 hours doing it. Successful applications show us that 2 or 3 hours is more than enough to cover all the evaluation points below.

## Constraints
- Use typescript 
- Use react-hooks
- Feel free to reuse any part of code or component already in the project
- Comment the code only when you consider it necessary
- Do not leave any unused dependencies or scripts
- Do not mock API response in your repository

## Nice to have
If you feel you have time to express yourself more here's the list of few points to guide you
- Fields validation
- Graphical and responsiveness improvements
- Improvement of existing code

## Evaluation points
- use of community best practices 
- use of clean code which is self documenting
- tests for business logic
- clean and extendable project structure, usage of best practices
- use of code quality checkers such as linters and build tools
- use of git with appropriate commit messages

## Results 
Please create a branch from the master one naming it in the format "name-surname".


## Available Scripts
In the project directory, you can run:

### `yarn start`
Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
